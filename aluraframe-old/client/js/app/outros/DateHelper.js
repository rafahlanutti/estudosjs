class DateHelper {
    
    constructor(){
        throw new Error("DateHelper Error. Classe não pode ser instanciada");
    }
 
 
       //  let data = new Date(this._inputData.value.replace(/-/g, ',')); //regex em js remove '-'
        // ...spread operator passa os parametros do vetor para a função new Date
            //map assa em cada elemento do vector e atribui a função passda por parameto
            //pode-se utilizar condições e fazer diversas operações dentro do mesmo
            //arrow function  => remove a palavra function, não precisa dar retorno, diminuição dos blocos
            //arrow function  => remove a palavra function, não precisa dar retorno, diminuição dos blocos
   static textoParadata(texto) {
    
    if(!/\d{4}-\d{2}-\d{2}/.test(texto)) {
        throw new Error("Formado de data errado");
    } 
         
    
    return new Date(...
            texto
            .split('-')
            .map((item, indice) => item - indice % 2));

            
    }

    static dataParaTexto(data) {
              
        return `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;
        //interprolação ${coloca váriavel em um templatestring}

    }
    

}