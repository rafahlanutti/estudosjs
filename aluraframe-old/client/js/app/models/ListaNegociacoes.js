class ListaNegociacoes{

    constructor(){
        this._negociacoes = [];
    }

    adicionar(negociacao) {
        this._negociacoes.push(negociacao);
    }
    get negociacoes(){
        return [].concat(this._negociacoes);
        //programação defensiva retorna uma copia das negocições para nao mudar as originais.
    }

}