class NegociacaoController {

    constructor() {
        let $ = document.querySelector.bind(document); //força que o document fique fixado no queryselector
        //Se não querySelector perde o foco.
        this._inputData = $('#data');
        this._inputQuantidade = $('#quantidade');
        this._inputValor = $('#valor');
        this._listaNegociacoes = new ListaNegociacoes();
        this._negociacoesView = new NegociacoesView($('#negociacoesView'));
        this._negociacoesView.update(this._listaNegociacoes);
        this._mensagem = new Mensagem();
        this._mensagemView = new MensagemView($('#mensagemView'));
        this._mensagemView.update(this._mensagem);
    }
    //função sem arrow function
    // adiciona(event) {
    //     event.preventDefault();

    //   let data = new Date(this._inputData.value.replace(/-/g, ',')); //regex em js remove '-'
    //   let data = new Date(...
    //     this._inputData.value
    //     .split('-')
    //     .map(function(item, indice) {
    //         return item  - indice % 2;
    //     }) 
    // );
    //função com arrow function
    adiciona(event) {
        event.preventDefault();

        this._listaNegociacoes.adicionar(this._criaNegociacao());
        this._negociacoesView.update(this._listaNegociacoes);
        
        this._mensagem.texto = "Evento adicionado com sucesso";  
        this._mensagemView.update(this._mensagem);
     
        this._limpaFormulario();

    }
    _criaNegociacao(){

       return new Negociacao(
         DateHelper.textoParadata(this._inputData.value),
         this._inputQuantidade.value,
         this._inputValor.value   
        );
    }
    _limpaFormulario(){
        this._inputData.value = '';
        this._inputQuantidade.value = 1;
        this._inputValor.value = 0;
        this._inputData.focus();
       
    }
   

}